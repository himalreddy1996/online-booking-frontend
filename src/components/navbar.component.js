import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import "../stylesheets/navbar.css"

export default class Navbar extends Component {

  constructor(props){
    super(props);
    this.state = {
      isUserLoggedIn: false,
    }
  }
  componentWillMount(){
    console.log("component will mount");

    let userData = localStorage.getItem("instacar_userdata");
    if(userData == null){
      this.setState({
        isUserLoggedIn: false
      })
    }else{
      this.setState({
        isUserLoggedIn: true
      })
    }
  }
  componentDidMount(){
    try{
      let path =window.location.pathname;
      let index = path.lastIndexOf("/");
      let trip = path.substr(index + 1);
      if(trip != ""){
        let dom = document.querySelector("."+trip);
        if(dom != null)
          dom.classList.add("active")
        }
    }catch(e){
      console.log(e);
    }
  }
  UserState() {
    if(this.state.isUserLoggedIn == true){
      return <Link to="/qa-index/logout" className="nav-link logout">Logout</Link>
    }else{
      return <Link to="/qa-index/login" className="nav-link login">Login</Link>
    }
  }
  onNavigatetoLink(e){
    document.querySelectorAll(".navbar-item .nav-link").forEach(function(x, y){
        x.classList.remove("active");
    });
    e.target.classList.add("active");
  }

  render() {
    {
          console.log(this.context);
    }
    return (
      <nav className="navbar navbar-dark bg-dark navbar-expand container">
        <div className="collapse navbar-collapse">
          <ul className="navbar-nav mr-auto">
            <li className="navbar-item" onClick={this.onNavigatetoLink}>
              <Link to="/qa-index/one-way" className="nav-link one-way">One Way</Link>
            </li>
            <li className="navbar-item" onClick={this.onNavigatetoLink}>
              <Link to="/qa-index/round-trip" className="nav-link round-trip">Round Trip</Link>
            </li>
            <li className="navbar-item" onClick={this.onNavigatetoLink}>
              <Link to="/qa-index/multicity" className="nav-link multicity">Multicity</Link>
            </li>
            <li className="navbar-item" onClick={this.onNavigatetoLink}>
              <Link to="/qa-index/package" className="nav-link package">Package</Link>
            </li>
            <li className="navbar-item userstate">
              { this.UserState() }
            </li>
          </ul>
        </div>
      </nav>
    );
  }
}
